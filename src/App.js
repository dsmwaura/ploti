// import logo from "./logo.svg";
import React from 'react'

import "./App.css";

import "./index.css";


import Navigation from "./components/Navigation";
import SearchInput from "./components/SearchInput";

import VacancyListing from "./components/VacancyListing";
// import {} from "./plotiMock.json" ;
import { useState, useEffect } from "react";
// import { getVacancies } from "./services/rentalDetails";

import { BrowserRouter as Router, Route, Routes } from "react-router-dom";


function App() {
  const [vacancies, setVacancies] = useState([]);
  const [locations, setLocations] = useState([]);

  useEffect(() => {
    getRegions()
    
  },[]);

  const getRegions = async() => {
    // const res = await fetch("./plotiMock.json")
    const headers ={
      "Content-Type": "application/json"
    }
    const res = await fetch(`/api/method/facility_management_website.api.tenant_renting.get_vacant_properties_with_locations`, headers);
    let propertyList = await res.json();
    const payLoad = propertyList.message
    const locations = payLoad.map(row=>{return row.location})
    const uniq = [...new Set(locations)]
    setVacancies(payLoad)
    setLocations(uniq)
}
/*
Obviously the naming pattern in this whole app needs to be relooked into
*/
  
  const mSearchInput = (
    <SearchInput
      className="box"
      placeholder="Search for your next living..."
      data={vacancies}
      locations={locations}
    />
  );
  return (
    <div className="container hero">
      <Navigation />
      <Router>
     
        <Routes>
        
          <Route path="/" exact element={mSearchInput} />
          <Route path="/listings" exact element={ <VacancyListing  /> } />
          <Route path="/listings/:location" exact element={ <VacancyListing/> } />
        </Routes>
      </Router>
    </div>
  );
}
export default App;
