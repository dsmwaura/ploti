import React from "react";
import { Navbar, Nav, NavDropdown, Container } from "react-bootstrap";
import plotiLogo from "../images/favicon_ploti.png"
import {Link} from 'react-router-dom'
import "../index.css"
const Navigation = () => {
  return (
    <Navbar collapseOnSelect expand="lg" sticky="top">
      <Container>
        <Navbar.Brand href="/"><img src={plotiLogo} className="brandImage" alt="Ploti.Cloud logo"/>  Ploti.Cloud</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">
            {/* <Nav.Link href="#features">Features</Nav.Link>
            <Nav.Link href="#pricing">Pricing</Nav.Link> */}
            
          </Nav>
          <Nav>
          <NavDropdown title="Applications" id="collasible-nav-dropdown">
              <NavDropdown.Item href="#action/3.1">Tenant Portal</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.2">
                Credit Report
              </NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3">General Queries</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="#action/3.4">
                My Statements
              </NavDropdown.Item>
            </NavDropdown>
            <Nav.Link href="#">Landlord Portal</Nav.Link>
            
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default Navigation;
