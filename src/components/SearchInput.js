import React from "react";
import "../index.css";
import { useState } from "react";

import "./SearchInput.css";

import {BiCurrentLocation} from 'react-icons/bi';
import {TiDeleteOutline} from 'react-icons/ti'
import {Link} from 'react-router-dom'


const defaultRegion =<p className="dataItem"> { <BiCurrentLocation style={{'color':'red','margin':'15px','font-size':'30px'}}/> }  Use your current location</p>

const SearchInput = ({ placeholder, data, locations }) => {
  const [uniqueLocation, setUniqueLocation] = useState([]);
  const [wordEntered, setWordEntered] = useState("");

  // console.log("We have data as ",data)
 
 
  const handleClearSearch=(e)=>{
    setWordEntered("")
    setUniqueLocation([])
  }
  const handleSearch = (e) => {

    const searchWord = e.target.value;

    setWordEntered(searchWord);

    const newFilter = locations.filter((value) => {
      return value.toLowerCase().includes(searchWord.toLowerCase());
    });

    console.log(wordEntered,uniqueLocation,newFilter)
    if(searchWord===""){
      setUniqueLocation([]) 
    }else{
      setUniqueLocation(newFilter);
    }
    // wordEntered === "" ? setFilteredData([]) : setFilteredData(newFilter);
  };

  return (
    <div className="col-sm container box">
      <h1 className="cta">
        Find your
        <br /> new start.
      </h1>
      <h3>Main Houses, Apartments and Condos for rent</h3>
      <hr />
      <form className="form-responsive">
      <div className="position-relative"><input type="text" placeholder={placeholder}  value={wordEntered} onChange={handleSearch}  /> { false  && <TiDeleteOutline style={{'font-size':'20px','color':'red'}} onClick={handleClearSearch}/>} </div>

        {uniqueLocation.length > 0 && (
          <div className="dataResult">
            {defaultRegion}
            { uniqueLocation.map((location, key) => {
              return (
                <p key={key} className="dataItem">
                  <Link className="routeLink" to={`/listings/${location}`} state={data.filter((row)=>row.location===location)}>{location}</Link> 
                </p>
              );
            })}
          </div>
         )}

         {(uniqueLocation.length === 0 && wordEntered.length>=1)  && (
          <div className="dataResult">
            {defaultRegion}
            
          </div>
         )}
         
      </form>{" "}
    </div>
  );
};

export default SearchInput;
