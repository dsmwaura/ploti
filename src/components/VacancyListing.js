import React from "react";
import { useParams, useLocation } from "react-router-dom";
import { Card, Row, Col, ListGroup, Modal, Form, Button } from "react-bootstrap";
import defaultHouseImage from "../images/for_rent.svg";
import { useEffect, useState } from "react";
import "./VacancyListing.css";
import { FaLocationArrow } from "react-icons/fa";
import { BsWifi } from "react-icons/bs";
import { BiWifiOff } from "react-icons/bi";
import PropertyCarousel from "./PropertyCarousel";
import RentalTourForm from "./RentalTourForm";
import {FiPhoneCall} from 'react-icons/fi'
const VacancyListing = () => {
  const [vacants, setVacants] = useState([]);
  const { location } = useParams();
  const { state } = useLocation();
  const [show, setShow] = useState(false);
  const [selectedVacant, setSelectedVacant] = useState({});

  useEffect(() => {
    setVacants(state);
  }, []);

  const imgStyle = {
    width: "100%",
    height: "60%",
  };

  const floatToCurrency = (amount, currency) => {
    return new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: currency || "USD",
    }).format(amount);
  };
  const handleLocationClick = (rentalUnit) => {
    const coordinates = rentalUnit.actual[0];
    console.log(coordinates.latitude, coordinates.longitude, rentalUnit);
    // https://www.google.com/maps/search/?api=1&query=47.5951518%2C-122.3316393
    //Read this: https://developers.google.com/maps/documentation/urls/get-started#search-action
    window.open(
      `https://www.google.com/maps/search/?api=1&query=${coordinates.latitude}%2C${coordinates.longitude}`
    );
  };
  function handleShow(rentalUnit) {
    setSelectedVacant(rentalUnit);
    
    setShow(true);
  }

  return (
    <>
      <h3> Apartments for rent in {location}</h3>
      <br />
      <br />

      <Row xs={1} md={3} className="g-4">
        {vacants.map((rentalUnit, idx) => (
          <Col key={idx}>
            <Card className="propertyCard">
              <Card.Img
                variant="top"
                src={rentalUnit.house_image || defaultHouseImage}
                style={imgStyle}
              />
              <Card.Body>
                <Card.Title>
                  {floatToCurrency(
                    parseFloat(rentalUnit.rental_rate || 0.0),
                    "KES"
                  )}
                  <FaLocationArrow
                    className="locationArrow"
                    onClick={() => {
                      handleLocationClick(rentalUnit);
                    }}
                  />
                  {rentalUnit.free_wifi.toLowerCase().includes("yes") ? (
                    <BsWifi className="wifiAccess" style={{ color: "green" }} />
                  ) : (
                    <BiWifiOff
                      className="wifiAccess"
                      style={{ color: "red" }}
                    />
                  )}
                </Card.Title>
                <h4>
                  {" "}
                  {rentalUnit.title || rentalUnit.name || ""} at{" "}
                  {rentalUnit.property_group || ""}{" "}
                </h4>
                <em></em>
                <Card.Text>
                  <h4>{rentalUnit.location}</h4>
                  <ListGroup horizontal>
                    <ListGroup.Item>{rentalUnit.bedroom} Bed</ListGroup.Item>
                    <ListGroup.Item>{rentalUnit.bathroom} Bath</ListGroup.Item>
                    <ListGroup.Item>{rentalUnit.parking}</ListGroup.Item>
                  </ListGroup>
                </Card.Text> 
                <button
                  className="btn btn-success btn-sm"
                  onClick={() => {
                    handleShow(rentalUnit);
                  }}
                >
                  Request a Tour
                </button>
                <button className="btn btn-secondary btn-sm">Message Us</button>
              </Card.Body>
            </Card>
          </Col>
        ))}
      </Row>

      <Modal show={show} fullscreen={true} onHide={() => setShow(false)}>
        <Modal.Header closeButton>
          <Modal.Title
            style={{ position: "absolute", right: "50%", color: "gray" }}
          >
            {floatToCurrency(
              parseFloat(selectedVacant.rental_rate || 0.0),
              "KES"
            )}
          </Modal.Title>
         
        </Modal.Header>
        <Modal.Body>
        <small style={{ color: "blue" }}>
            {" "}
            {selectedVacant.name}, {selectedVacant.location}{" "}
          </small>
          <PropertyInfo selectedProperty={selectedVacant} />
        </Modal.Body>
      </Modal>
    </>
  );
};

// import React from 'react'

const PropertyInfo = ({ selectedProperty }) => {
  const [propertyPhotographs, setPropertyPhotographs] =useState([])
  useEffect(() =>{
    const defaultCarousel =[ {
      "photo": selectedProperty.house_image || defaultHouseImage,
      "description": `Property photograph for ${selectedProperty.name}`
    }]
    console.log(selectedProperty.property_photographs, "are the photos")
    setPropertyPhotographs([])
    selectedProperty.property_photographs.length > 0  ? setPropertyPhotographs(selectedProperty.property_photographs ) : setPropertyPhotographs(defaultCarousel)
  },[])
  return (
    <div className="row">
      
      <div className="col-sm-8">
        <div className="row">

          <PropertyCarousel propertyName={selectedProperty.name} propertyImages={propertyPhotographs}  style={{ "max-height":"500px", "display": "flex" }} />
          
        </div>
        <div className="row">

        </div>
      </div>
      <div className="col-sm-4">
        <div>
          <h5 style={{"margin-top":"20px"}}><FiPhoneCall/> Call us: +222-222-222</h5>
        </div>
        <RentalTourForm/>
      </div>
    </div>
  );
};


export default VacancyListing;
