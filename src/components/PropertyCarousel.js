import React from "react";

// import useEffect from 'react'
import {Carousel} from 'react-bootstrap'


const PropertyCarousel = ({ propertyName, propertyImages }) => {
  // console.log(match);
  return (
    <div>
      <h3> Property Images for {propertyName} </h3>

        <Carousel variant="light" fade>
        { propertyImages.map((photoImg, idx) =>  (
        <Carousel.Item>
          <img
            className="d-block w-100"
            src={photoImg.photo}
            alt={photoImg.description || `Property photograph for ${propertyName}`}
            style={{"display": "block","width": "100%", "max-height":"500px","object-fit": "cover"}}
          />
          <Carousel.Caption>
            
            <h5>{photoImg.description}</h5>
          </Carousel.Caption>
        </Carousel.Item>
        ))}
      </Carousel>
     
      
    </div>
  );
};

export default PropertyCarousel;
