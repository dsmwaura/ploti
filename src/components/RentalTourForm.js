import React from "react";
import { Form, Button } from "react-bootstrap";

const RentalTourForm = () => {
  const handleSubmit = (e) => {
    e.preventDefault();
  };
  return (
    <div>
      <Form>
        <Form.Control type="text" placeholder="Normal text" />
        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
          <Form.Label>Email address</Form.Label>
          <Form.Control type="email" placeholder="name@example.com" />
        </Form.Group>
        <Form.Control type="text" placeholder="Normal text" />
        <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
          <Form.Label>Example textarea</Form.Label>
          <Form.Control as="textarea" rows={3} />
        </Form.Group>
        <Button variant="primary" type="submit" onClick={handleSubmit}>
          Submit{" "}
        </Button>{" "}
      </Form>{" "}
    </div>
  );
};

export default RentalTourForm;
